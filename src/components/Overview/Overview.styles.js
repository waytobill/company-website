import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {
    // height: '90vh'
  },
  content: {
    height: '40vh',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  image1: {
    height: '30vh',
    border: '1px solid grey'
  }
}));