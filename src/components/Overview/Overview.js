import React from 'react';
import { Typography } from 'kill-components';

import useStyles from './Overview.styles';

export default function Overview() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.image1}></div>
            <div className={classes.content}>
                <Typography variant="body1">In the checkout, the customer gets a clear overview before the purchase and a confirmation mail after the purchase.</Typography>
                <Typography variant="body1">The simple and transparent buying process provides trust between you and your customer.</Typography>
            </div>
            <div className={classes.image1}></div>
        </div>
    )
}
