import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {
    height: '100vh',
    backgroundColor: 'lightgrey',
    weight: '100%'
  }
}));