import React from 'react';
import { Typography } from 'kill-components';

import useStyles from './Benefits.styles';

export default function Benefits() {
    const classes = useStyles();
    return (
        <div className={classes.benefits}>
            <div className={classes.box}>
                <div className={classes.image}></div>
                <Typography variant="h5">VALUE</Typography>
                <Typography variant="body1" align="center">Decrease the number of cancelations and increases the payment rate and the value of each sales</Typography>
            </div>
            <div className={classes.box}>
                <div className={classes.image}></div>
                <Typography variant="h5">VALUE</Typography>
                <Typography variant="body1" align="center">Decrease the number of cancelations and increases the payment rate and the value of each sales</Typography>
            </div>
            <div className={classes.box}>
                <div className={classes.image}></div>
                <Typography variant="h5">VALUE</Typography>
                <Typography variant="body1" align="center">Decrease the number of cancelations and increases the payment rate and the value of each sales</Typography>
            </div>
        </div>
    )
}
