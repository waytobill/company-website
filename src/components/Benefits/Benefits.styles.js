import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {

  },
  box: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  benefits: {
    height: '90vh',
    backgroundColor: 'lightgrey',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  image: {
    height: '50px',
    width: '50px',
    border: '1px solid grey'
  }
}));