import React from 'react';
import { Typography, Button } from 'kill-components';

import useStyles from './Landing.styles';

export default function Landing() {
    const classes = useStyles();

    return (
        <div className={classes.landing}>
            <Typography variant="h3" className={classes.header}>The way to bill with telesales</Typography>
            <Typography variant="h4" className={classes.subtitle}>Accept payments from your customers, effortless and easy, and improve your sales result.</Typography>
            <Button>Start Now</Button>
        </div>
    )
}
