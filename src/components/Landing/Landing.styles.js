import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {

  },
  landing: {
    textAlign: 'left',
    height: '80vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    padding: '2rem'
  },
  header: {
    fontSize: '2.6rem'
  },
  subtitle: {
    fontSize: '1.5rem'
  }
}));