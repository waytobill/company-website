import React from 'react';
import { Typography, Button } from 'kill-components';
import { Grid } from '@material-ui/core';

import useStyles from './Contact.styles';

export default function Contact() {
    const classes = useStyles();
    return (
        <Grid container className={classes.root}>
            <Grid className={classes.contact}>
                <Typography variant="h3" className={classes.header}>The way to bill with telesales</Typography>
                <Typography variant="h4" className={classes.subtitle}>Accept payments from your customers, effortless and easy, and improve your sales result.</Typography>
                <Button>Start Now</Button>
            </Grid>
            <Grid className={classes.trust}>
                <div className={classes.logoBox}>
                    <div className={classes.logo}></div>
                    <div className={classes.logo}></div>
                    <div className={classes.logo}></div>
                </div>
            </Grid>
        </Grid>
    )
}
