import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {
    
  },
  contact: {
    height: '60vh',
    border: '1px solid grey',
    width: '100%',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  trust: {
    height: '30vh',
    width: '100%',
    border: '1px solid grey',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoBox: {
    display: 'flex'
  },
  logo: {
      height: '80px',
      width: '80px',
      border: '1px solid grey'
  },
  header: {
    fontSize: '2rem'
  },
  subtitle: {
    fontSize: '1.5rem'
  }
}));