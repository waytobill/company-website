import React from 'react';
import { Typography } from 'kill-components';
import { Grid } from '@material-ui/core';

import useStyles from './Location.styles';

export default function Location() {
    const classes = useStyles();
    return (
        <Grid container className={classes.root}>
            <Grid item className={classes.map}></Grid>
            <Grid item className={classes.text}>
                <Typography variant="body1">7 countries, 1 payment provider</Typography>
                <Typography variant="body1">We operate on several markets in Europe and support you as you grow your business.</Typography>
            </Grid>
        </Grid>
    )
}
