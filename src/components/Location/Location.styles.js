import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {
    
  },
  map: {
    height: '50vh',
    width: '100%',
    backgroundColor: 'lightgrey',
    border: '1px solid grey'
  },
  text: {
    height: '30vh',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  }
}));