import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {
  },
  text: {
    height: '60vh',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  dialers: {
    height: '30vh',
    width: '100%',
    backgroundColor: 'lightgrey',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logoBox: {
    display: 'flex'
  },
  logo: {
      height: '80px',
      width: '80px',
      border: '1px solid grey'
  }
}));