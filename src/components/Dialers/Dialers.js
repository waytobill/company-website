import React from 'react';
import { Typography } from 'kill-components';
import { Grid } from '@material-ui/core';

import useStyles from './Dialers.styles';

export default function Dialers() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <Grid className={classes.text}>
                <Typography variant="body1">Easy to land the deal via access to payment button and follow-up options in your dialer.</Typography>
                <Typography variant="body1">Behind the scenes you have a full payment machinery working on behalf of you.</Typography>
                <Typography variant="body1">Support for onetime purchase, subscriptions, contracts, campaign settings</Typography>
            </Grid>
            <Grid className={classes.dialers}>
                <Typography variant="body1">Simple and easy integration</Typography>
                <div className={classes.logoBox}>
                    <div className={classes.logo}></div>
                    <div className={classes.logo}></div>
                    <div className={classes.logo}></div>
                </div>
                <Typography variant="body1">You don't find your dialer? Let us know.</Typography>
            </Grid>
        </Grid>
    )
}
