import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {

  },
  logo: {
      width: '150px',
      height: '50px',
      border: '1px solid grey'
  },
  appbar: {
    boxShadow: 'none',
    display: 'flex',
    '& .MuiToolbar-root': {
        justifyContent: 'space-between'
    }
  }
}));