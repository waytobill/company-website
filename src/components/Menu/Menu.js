import React from 'react';
import { AppBar, Button } from 'kill-components';

import useStyles from './Menu.styles';

export default function Menu() {
    const classes = useStyles();
    
    return (
            <AppBar className={classes.appbar} color="transparent">
                <div className={classes.logo}></div>
                <Button icon="menu"></Button>
            </AppBar>
    )
}
