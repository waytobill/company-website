import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  root: {
    height: '100vh'
  },
  screen: {
    height: '70vh',
    border: '1px solid grey'
  },
  text: {
    height: '20vh',
    padding: '2rem',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center'
  },
  subtitle: {
  }
}));