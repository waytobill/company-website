import React from 'react';
import { Typography } from 'kill-components';

import useStyles from './Screen.styles';

export default function Screen() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div className={classes.screen}></div>
            <div className={classes.text}>
                <Typography variant="subtitle1" className={classes.subtitle}>Make payments effortless and easy</Typography>
                <Typography variant="body1">Send sms-link with the checkout to the consumer directly from your dialer.</Typography>
            </div>
        </div>
    )
}
