import './App.css';
import Menu from './components/Menu/Menu';
import Landing from './components/Landing/Landing';
import Benefits from './components/Benefits/Benefits';
import Screen from './components/Screen/Screen';
import Overview from './components/Overview/Overview';
import Dialers from './components/Dialers/Dialers';
import Location from './components/Location/Location';
import Contact from './components/Contact/Contact';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <div className="App">
      <Menu />
      <div className="body">
        <Landing />
        <Benefits />
        <Screen />
        <Overview />
        <Dialers />
        <Location />
        <Contact />
        <Footer />
      </div>
    </div>
  );
}

export default App;
